# speech2wordcloud: Memetic Petri-Dish

Create wordclouds on the fly from words spoken into the mic. 
Uses Mozilla's DeepSpeech speech recognition toolkit and the excellent Python WordCloud library from amueller.


*Setup:*
1. Clone the repository.
2. Setup virtual environment and install dependencies based on requirements.txt (see: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/ )
3. Place Deepspeech pre-trained models in the repository folder in subfolder called "models" according to instructions on DeepSpeech setup section:
https://github.com/mozilla/DeepSpeech#getting-the-pre-trained-model
4. Run it!: python3 memetic-petri.py

Notes:
*heard-text.txt is read at open and written on close.

*Instructions:*
Start up and wait a bit while Deepspeech gets ready. 
* r = Reset wordcloud to empty.
* t = activate timer 
* F11 = Fullscreen



