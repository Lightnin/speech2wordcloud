from tkinter import *
from PIL import Image, ImageTk
from wordcloud import WordCloud
from DeepSpeechVAD import *
import os
import logging
import threading
import nltk

# Parts of speech to include in wordcloud via nltk parts of speech (pos) tagger.
# These are Nouns, verbs, and Adjectives
include_type_of_speech = ['NN', 'NNP', 'NNS', 'VB', 'VBD', 'VBG','VBN','VBP','VBZ', 'JJ', 'JJR', 'JJS']


class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.pack(fill=BOTH, expand=1)

        self.fullscreenstate = False
        self.timerrefresh = False
        self.listening = False

        # create a toplevel menu
        menubar = Menu(root,background="black", borderwidth=0, foreground = "white" )
        # create a pulldown menu, and add it to the menu bar
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Reload Cloud", command=self.showCloud)
        filemenu.add_command(label="Empty Cloud", command=self.reset_cloud)
        filemenu.add_separator()
        filemenu.add_command(label="Toggle FullScreen", command=self.toggle_fullscreen)
        filemenu.add_command(label="Toggle Timer refresh", command=self.toggle_timerrefresh)
        filemenu.add_separator()
        filemenu.add_command(label="Quit", command=self.on_closing)
        menubar.add_cascade(label="≡", menu=filemenu)

        # display the menu
        root.config(menu=menubar)

        load = Image.open("splash-screen.png")
        render = ImageTk.PhotoImage(load)
        img = Label(self, borderwidth=0,highlightthickness = 0, image=render)
        img.image = render
        img.place(x=0, y=0)


        #Open Mask image and resize if necessary to 1/2 screen dimensions.
        self.half_screen_width = int(round(self.winfo_screenwidth() / 2))
        self.half_screen_height = int(round(self.winfo_screenheight() / 2))
        mask_image = Image.open("circle_mask.png")

        # The mask is set to 960 x 540 already. If this screen isn't 1920 x 1080, resize it.
        if self.winfo_screenwidth() != 1920 or self.winfo_screenheight() != 1080:
            halfsize_mask = mask_image.resize((self.half_screen_width, self.half_screen_height), Image.BICUBIC)
        else:
            halfsize_mask = mask_image
        self.circle_mask = np.array(halfsize_mask)

        # Get the list of Stopwords that will be excluded from clouds
        self.stopwords = set(line.strip() for line in open('stopwords.txt'))

        # Load the wordlist for the current cloud
        try:
            txtfile = open("heard-text.txt", "r")
            self.heardtext = txtfile.read()
            txtfile.close()
        except:
            print("Heard-text.txt file open failed.")

        root.config(bg='black')
        root.focus_set()
        root.bind("t", self.toggle_timerrefresh)
        root.bind("<F11>", self.toggle_fullscreen)
        root.bind("<Escape>", self.end_fullscreen)
        #root.bind("<l>", self.toggleListening)
        root.bind("<r>", self.reset_cloud)

        self.pack(padx=20, pady=20)

        # Turn listening on
        self.toggleListening(self)

    def deepSpeechDaemon(self):
        # Arguments for DeepSpeech engine
        # For setup info see: https://github.com/mozilla/DeepSpeech#prerequisites
        MODEL = 'models/output_graph.pbmm'
        BEAM_WIDTH = 500
        DEFAULT_SAMPLE_RATE = 16000
        LM_ALPHA = 0.75
        LM_BETA = 1.85
        N_FEATURES = 26
        N_CONTEXT = 9
        LM = 'models/lm.binary'
        TRIE = 'models/trie'
        ALPHABET = 'models/alphabet.txt'
        VAD_AGGRESSIVENESS = 3
        DEVICE = None
        SAVEWAV = False
        NOSPINNER = False

        print('Initializing model...')

        model = deepspeech.Model(MODEL, N_FEATURES,  N_CONTEXT , ALPHABET, BEAM_WIDTH)
        model.enableDecoderWithLM(ALPHABET, LM, TRIE, LM_ALPHA, LM_BETA)

        # Start audio with VAD
        vad_audio = VADAudio(VAD_AGGRESSIVENESS,
                             DEVICE,
                             DEFAULT_SAMPLE_RATE)
        print("Listening (ctrl-C to exit)...")
        self.showCloud()
        frames = vad_audio.vad_collector()

        # Stream from microphone to DeepSpeech using VAD
        spinner = None
        if not NOSPINNER: spinner = Halo(spinner='line')
        stream_context = model.setupStream()
        wav_data = bytearray()
        for frame in frames:
            if frame is not None:
                if spinner: spinner.start()
                logging.debug("streaming frame")
                model.feedAudioContent(stream_context, np.frombuffer(frame, np.int16))
                if SAVEWAV: wav_data.extend(frame)
            else:
                if spinner: spinner.stop()
                logging.debug("end utterence")

                if SAVEWAV:
                    vad_audio.write_wav(
                        os.path.join(ARGS.savewav, datetime.now().strftime("savewav_%Y-%m-%d_%H-%M-%S_%f.wav")),
                        wav_data)
                    wav_data = bytearray()
                text = model.finishStream(stream_context)
                if text:
                    print("Heard: %s" % text)
                    if self.heardtext == "empty":
                        self.heardtext = " "
                    speech_tokens = nltk.pos_tag(nltk.word_tokenize(text))
                    print(speech_tokens)
                    print(end = "Added: ")
                    for k, v in speech_tokens:
                        if v in include_type_of_speech:
                            self.heardtext = self.heardtext + k + " "
                            print(k, " ",end =" ")
                    print("\n")
                    if not self.timerrefresh:
                        self.showCloud()

                stream_context = model.setupStream()


    def showCloud(self, event=None):
        # scale factor is 1 (with 960 size) or fullscreenstate + 1 = 2 when full screen.
        # Catch ValueError, which probably means it was called with text that got emptied by stopwords. Then just show empty.
        try:
            wordcloud = WordCloud(scale = self.fullscreenstate + 1, width = self.half_screen_width, height = self.half_screen_height, background_color="black", stopwords=self.stopwords, max_words=40, mask=self.circle_mask).generate(self.heardtext)
        except ValueError:
            print("Got a ValueError, so displaying \'empty\'")
            wordcloud = WordCloud(scale = self.fullscreenstate + 1, width = self.half_screen_width, height = self.half_screen_height, background_color="black", stopwords=self.stopwords, max_words=40, mask=self.circle_mask).generate("empty")


        load = wordcloud.to_image()
        render = ImageTk.PhotoImage(load)
        img = Label(self, borderwidth=0,highlightthickness = 0, image=render)
        img.image = render
        img.place(x=0, y=0)


    def toggleListening(self, event=None):
        self.listening = not self.listening
        if self.listening == True:
            self.deepspeechthread = threading.Thread(target=self.deepSpeechDaemon)
            self.deepspeechthread.daemon = True
            self.deepspeechthread.start()
        else:
            self.deepspeechthread.join()


    def toggle_fullscreen(self, event=None):
        self.fullscreenstate = not self.fullscreenstate  # Just toggling the boolean
        root.attributes("-fullscreen", self.fullscreenstate)
        self.showCloud()
        return "break"


    def end_fullscreen(self, event=None):
        self.fullscreenstate = False
        root.attributes("-fullscreen", False)
        self.showCloud()
        return "break"


    def toggle_timerrefresh(self, event=None):
        self.timerrefresh = not self.timerrefresh
        if self.timerrefresh == True:
            self.timer_refresh()
        return "break"


    def timer_refresh(self, event=None):
        if self.timerrefresh == True:
            self.showCloud()
            self.after(5000, self.timer_refresh)
        return "break"


    def reset_cloud(self, event=None):
        self.heardtext = "empty"
        self.showCloud()
        return "break"


    def on_closing(self):
        txtfile = open("heard-text.txt", "+w")
        txtfile.write(self.heardtext)
        txtfile.close()
        #Should clean up the threads, but that makes it all block.
        #if self.listening == True:
        #    self.toggleListening()
        root.destroy()


root = Tk()
app = Window(root)
root.wm_title("Memetic Petri Dish")
root.geometry('%sx%s' % (app.half_screen_width,app.half_screen_height))
root.configure(background='black')
root.protocol("WM_DELETE_WINDOW", app.on_closing)
root.mainloop()
