import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
from wordcloud import WordCloud
from os import path
import numpy as np
from PIL import Image

fig = plt.figure(figsize=(12, 8))


import sys
from datetime import datetime, timedelta

update_delta = timedelta(seconds=1)
last_update_time = datetime.now() - update_delta # -seconds so the animate() function updates immediately
paused = False

def keypress(event):
    global paused
    # code taken from https://matplotlib.org/examples/event_handling/keypress_demo.html
    # print('press', event.key)
    # sys.stdout.flush()
    if event.key == ' ':
        paused = not paused
        fig.canvas.draw()
fig.canvas.mpl_connect('key_press_event', keypress)




def animate(i):
    global last_update_time

    if paused or (datetime.now() - last_update_time < update_delta):
        return

    circle_mask = np.array(Image.open("circle_mask.png"))

    text = open("heard-text.txt","r").read()
    wordcloud = WordCloud(width=1280, height=720, background_color="black",max_words=40,mask=circle_mask).generate(text)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    # This will render the image as a png and get rid of most, but not all, the white border. (See Issue #1)
    # https://stackoverflow.com/questions/9622163/save-plot-to-image-file-instead-of-displaying-it-using-matplotlib
    # plt.savefig('cloud-test-image.png', bbox_inches='tight')
    last_update_time = datetime.now()

# This will reload the function every second.
ani = animation.FuncAnimation(fig, animate, interval=1)

plt.show()